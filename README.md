# Lab: Face recognition using PCA
This nootebook contains a PYTHON implementation of Face Recognition from images , based on Principal Components Analysis (PCA). This Lab demonstartes the use of Principal Component Analysis, a method of dimensional reduction in order to help us create a model for Facial Recognition. The idea is to project faces onto a feature space which best encodes them, these features spaces mathematically correspond to the eigen vector space of these vectors. We then use the following projections along with Machine Learning techniques to build a Facial Recognizer

We will be using Python to help us develop this model

@inproceedings{Turk1998, Author = {M. A. Turk and A. P. Pentland}, title = {Face recognition using eigenfaces}, booktitle = {Proceedings. 1991 IEEE Computer Society Conference on Computer Vision and Pattern Recognition}, year = {1998}, }

#Demo:

To run the demo, you should complete all following TODO. The Face Recognition system includes the following main steps:

1. Compute the mean face
2. Data normalization
3. PCA construction
4. Compute the retained eigenvectors
5. Projection on the PCA space
6. Test of a query image
